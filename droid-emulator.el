;;; droid-emulator.el --- Run the Android emulator   -*- lexical-binding: t; -*-

;; Copyright (C) 2019  Alessio Vanni

;; Author: Alessio Vanni <vannilla@firemail.cc>
;; Keywords: convenience, processes

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Run the Android emulator

;;; Code:
(require 'droid-custom)

(defun droid-emulator-start (avd)
  "Start the emulator.
AVD is the name of an existing virtual device."
  (interactive "MAVD name: ")
  (unless droid-emulator-path
    (error "`droid-emulator-path' is not set"))
  (make-process :name "Android Emulator"
		:command (list (expand-file-name (concat droid-emulator-path
							 "/emulator"))
			       "-avd"
			       avd)))

(provide 'droid-emulator)
;;; droid-emulator.el ends here
