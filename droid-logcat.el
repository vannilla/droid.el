;;; droid-logcat.el --- Read Android's application log  -*- lexical-binding: t; -*-

;; Copyright (C) 2018  Alessio Vanni

;; Author: Alessio Vanni <vannilla@firemail.cc>
;; Keywords: 

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; These functions allow to read the application log through the use of
;; logcat.  Currently Droid will start a logcat process that will listen
;; to the connected device for any log output.

;;; Code:
(require 'droid-custom)

(defconst droid--logcat-command "adb"
  ;; As of now, logcat is invoked through adb, so
  ;; the actual logcat command will be included into
  ;; the arguments list
  "Command to call to run logcat.")

(defconst droid--logcat-base-arguments "logcat"
  "Arguments required to run logcat.")

(defconst droid-logcat-buffer-name "*Logcat*"
  "Buffer name with logcat's output.")

(defvar droid--logcat-process nil
  "Logcat process.")

(defvar droid-logcat-mode-map
  (let ((map (make-sparse-keymap)))
    (define-key map (kbd "q") #'droid-logcat-stop)
    (define-key map (kbd "f") #'droid-logcat-filter)
    map)
  "Logcat mode key map.")

(defun droid--logcat-process-filter (proc str)
  "Read output of PROC, passed as STR, and operate on it."
  (when (buffer-live-p (process-buffer proc))
    (with-current-buffer (process-buffer proc)
      (save-excursion
	(goto-char (process-mark proc))
	(insert (subst-char-in-string ? 32 str))
	(set-marker (process-mark proc) (point)))
      (goto-char (process-mark proc)))))

(defmacro droid--logcat-build-process-command (cmd)
  "Use CMD as the process command."
  `(make-process :name "Logcat"
		 :buffer droid-logcat-buffer-name
		 :filter #'droid--logcat-process-filter
		 :command ,cmd
		 :connection-type 'pipe))

(defun droid-logcat-start ()
  "Start a logcat process.
Only one logcat process can be run."
  (interactive)
  (unless droid--logcat-process
    (let ((logcat (expand-file-name (concat droid-sdk-path
					    "/platform-tools/"
					    droid--logcat-command))))
      (with-current-buffer (get-buffer-create droid-logcat-buffer-name)
	(droid-logcat-mode))
      (setq droid--logcat-process
	    (droid--logcat-build-process-command
	     (list logcat droid--logcat-base-arguments)))
      (pop-to-buffer droid-logcat-buffer-name))))

(defun droid-logcat-stop ()
  "Stop the running logcat process."
  (interactive)
  (when droid--logcat-process
    (delete-process droid--logcat-process)
    (setq droid--logcat-process nil)))

(defun droid-logcat-filter (filter)
  "Filter the logcat output according to logcat's rules.

This stops the currently running process and clears the buffer contents."
  (interactive "MFilter: ")
  (unless droid--logcat-process
    (error "No running logcat process"))
  (droid-logcat-stop)
  (with-current-buffer droid-logcat-buffer-name
    (delete-region (point-min) (point-max)))
  (let ((logcat (expand-file-name (concat droid-sdk-path
					  "/platform-tools/"
					  droid--logcat-command))))
    (setq droid--logcat-process
	  (droid--logcat-build-process-command
	   (list logcat droid--logcat-base-arguments
		 filter)))
    (pop-to-buffer droid-logcat-buffer-name)))

(define-derived-mode droid-logcat-mode fundamental-mode "Logcat"
  "Major mode for logcat's output.

\\{droid-logcat-mode-map}")

(provide 'droid-logcat)
;;; droid-logcat.el ends here
