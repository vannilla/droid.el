;;; droid-custom.el --- defcustoms for Droid         -*- lexical-binding: t; -*-

;; Copyright (C) 2018  Alessio Vanni

;; Author: Alessio Vanni <vannilla@firemail.cc>
;; Keywords: 

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; This file contains Droid's defcustom definitions.

;;; Code:
(defgroup droid nil
  "Android development environment."
  :group 'tools
  :prefix "droid-")

(defcustom droid-sdk-path nil
  "Path to the Android SDK.
The path should point to the directory containing the tools and
platform-tools directories.

This variable doesn't track the session
environment (e.g. ANDROID_HOME), so if the SDK is moved make sure
to keep this value in sync."
  :package-version '(Droid . "1.0")
  :type '(directory)
  :group 'droid)

(defcustom droid-assets-path
  (if load-file-name
    (concat (file-name-directory load-file-name)
	    "assets/")
    nil)
  "Path to Droid's default asssets, e.g. icons.
The assets in this path will be used as the default assets in a
new project."
  :package-version '(Droid . "1.0")
  :type '(directory)
  :group 'droid)

(defcustom droid-emulator-path nil
  "Path to the emulator executable.
The path should point to a directory containing the \"emulator\" executable."
  :package-version '(Droid . "1.4")
  :type '(directory)
  :group 'droid)

(provide 'droid-custom)
;;; droid-custom.el ends here
