;;; droid-project.el --- Functions working on the project structure  -*- lexical-binding: t; -*-

;; Copyright (C) 2018  Alessio Vanni

;; Author: Alessio Vanni <vannilla@firemail.cc>
;; Keywords: 

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; These functions deal with the structure of a project directory
;; tree.

;;; Code:
(defconst droid--project-file-name ".droid"
  "File used to check if a directory tree is a Droid project.")

(defvar droid--tasks-list '()
  "List of available Droid tasks.")

(defvar droid--tasks-history '()
  "History of executed tasks.
Used in `completing-read'.")

(defmacro with-droid-default-directory (dir &rest body)
  "Make DIR the current directory and evaluate BODY."
  (declare (indent 1) (debug t))
  `(let ((default-directory ,dir))
     ,@body))

(defun droid--project-root ()
  "Find the project's root directory."
  (let ((dir (locate-dominating-file default-directory
				     droid--project-file-name)))
    (unless dir
      (error "Can't find project root"))
    dir))

;;;###autoload
(defmacro deftask-droid (name &rest body)
  "Define a new Droid task.
NAME is the task name, BODY is the task's behaviour (like a
`defun' body).  The new task will automatically execute in the
project root directory.  If the task executes `compile', it
guarantees to never change `compile-command'."
  (declare (debug t) (indent 1))
  (let* ((old (make-symbol "old"))
	 (nname (if (symbolp name) (symbol-name name) name))
	 (fname (intern (concat "droid-task-" nname))))
    `(let ((strname ,nname))
       (push strname droid--tasks-list)
       (defun ,fname ()
	 "A Droid task."
	 (with-droid-default-directory (droid--project-root)
	   (let ((,old compile-command))
	     ,@body
	     (setq compile-command ,old)))))))

;;;###autoload
(defun droid-run-task ()
  "Run a Droid task."
  (declare (interactive-only t))
  (interactive)
  (let ((task (downcase (completing-read "Task: " droid--tasks-list nil t
					 nil
					 'droid--tasks-history))))
    (let ((fun (intern (concat "droid-task-" task))))
      (unless (fboundp fun)
	(error "Task %s is not supported by Droid" task))
      (funcall fun))))

;;;###autoload
(deftask-droid assemble-debug
  (compile "./gradlew assembleDebug"))

;;;###autoload
(deftask-droid assemble-release
  (compile "./gradlew assembleRelease"))

;;;###autoload
(deftask-droid install-debug
  (compile "./gradlew installDebug"))

;;;###autoload
(deftask-droid install-release
  (compile "./gradlew installRelease"))

(provide 'droid-project)
;;; droid-project.el ends here
