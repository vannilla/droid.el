;;; droid.el --- Android development environment     -*- lexical-binding: t; -*-

;; Copyright (C) 2018  Alessio Vanni

;; Author: Alessio Vanni <vannilla@firemail.cc>
;; Version: 1.4
;; Package-Requires: ((emacs "25.3"))
;; Keywords: convenience, processes

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Droid is a development environment for Android applications.
;; `droid-create-project' will create a new directory tree containing
;; an application project.  The structure is meant to be compatible
;; with Android Studio's projects.  When visiting any of the project's
;; files, it's possible to execute a task such as compiling the
;; application or installing it on a device, using `droid-run-task'.
;; That function also offers symbol completion.
;;
;; To be usable, Gradle and the full Android SDK are needed, so make
;; sure to install them first.  Then set `droid-sdk-path' to the
;; absolute path to the SDK.

;;; Code:
(require 'droid-custom)
(require 'droid-files)
(require 'droid-project)
(require 'droid-emulator)

(defconst droid--sdkmanager-path "tools/bin/sdkmanager"
  "Path to the sdkmanager executable, relative to `droid-sdk-path'.")

(defun droid--package-to-path (name)
  "Change NAME from a Java-like package name to a directory tree."
  (let ((split (split-string name "\\." t))
	(path ""))
    (dolist (p split)
      (setq path (concat path p "/")))
    path))

(defun droid--list-targets ()
  "Create a list of installed Android targets."
  (with-temp-buffer
    (let ((sdkmanager nil))
      (let ((i 0)
	    (tmp nil))
	(while (and (not sdkmanager) (< i (length exec-suffixes)))
	  (setq tmp (concat (expand-file-name droid-sdk-path)
			    "/" droid--sdkmanager-path (nth i exec-suffixes)))
	  (when (file-exists-p tmp)
	    (setq sdkmanager tmp))
	  (setq i (1+ i))))
      (unless sdkmanager
	(error "Can't find the sdkmanager tool"))
      ;; No cache here because users can install new targets any time
      (message "Building list of installed targets...")
      (call-process sdkmanager nil t nil "--list")
      (message "Building list of installed targets...done"))
    (goto-char (point-min))
    (let ((targets ()))
      (while (re-search-forward "platforms/android-\\([0-9]+\\)" nil t)
	(push (match-string 1) targets))
      (unless targets
	(error "No targets found"))
      (setq targets (nreverse targets))
      targets)))

;;;###autoload
(defun droid-create-project ()
  "Create a new application project.
The operations required to create a new project might take some
time and block Emacs, so make sure to call this function when
nothing important is happening."
  (interactive)
  (unless droid-sdk-path
    (error "`droid-sdk-path' is not set"))
  (unless droid-assets-path
    (error "`droid-assets-path' is not set"))
  (let ((app-name (read-string "Project name: "))
	(app-package (read-string "Package: "))
	(app-path (expand-file-name
		   (read-directory-name "Project path: "
					nil default-directory)))
	(app-version (read-number "Target: " 23)))
    (when (file-exists-p app-path)
      (with-droid-default-directory app-path
	(when (file-exists-p droid--project-file-name)
	  (error "%s already contains a project" app-path))))
    (unless (file-exists-p app-path)
      (make-directory app-path t))
    (with-droid-default-directory app-path
      (with-temp-file droid--project-file-name
	(insert "Made with Droid") (newline)
	(insert "Don't place this file under version control")
	(newline))
      (message "Initializing Gradle...")
      (call-process "gradle" nil nil nil "init")
      (message "Initializing Gradle...done")
      (make-directory "app")
      (droid-files-create-build-gradle)
      (droid-files-create-gradle-properties)
      (droid-files-create-settings-gradle)
      (droid-files-create-local-properties droid-sdk-path))
    (with-droid-default-directory (concat app-path "/app")
      (make-directory "libs" t)
      (make-directory "src/main" t)
      (droid-files-create-app-build-gradle app-version app-package)
      (droid-files-create-app-proguard-rules))
    (with-droid-default-directory (concat app-path "/app/src/main")
      (make-directory (concat "java/" (droid--package-to-path app-package)) t)
      (make-directory "res/" t)
      (droid-files-create-manifest app-package))
    (with-droid-default-directory (concat app-path "/app/src/main/java/"
					  (droid--package-to-path app-package))
      (droid-files-create-activity app-package))
    (with-droid-default-directory (concat app-path "/app/src/main/res/")
      (make-directory "drawable" t)
      (make-directory "drawable-v24" t)
      (make-directory "layout" t)
      (make-directory "mipmap-anydpi-v26" t)
      (make-directory "mipmap-hdpi" t)
      (make-directory "mipmap-mdpi" t)
      (make-directory "mipmap-xhdpi" t)
      (make-directory "mipmap-xxhdpi" t)
      (make-directory "mipmap-xxxhdpi" t)
      (make-directory "values" t))
    (with-droid-default-directory (concat app-path "/app/src/main/res/layout")
      (droid-files-create-layout-xml))
    (with-droid-default-directory (concat app-path "/app/src/main/res/values")
      (droid-files-create-string-xml app-name)
      (droid-files-create-style-xml))
    (dolist (r (list "mdpi" "hdpi" "xhdpi" "xxhdpi" "xxxhdpi"))
      (with-droid-default-directory (concat app-path
					    "/app/src/main/res/mipmap-"
					    r)
	(copy-file (concat droid-assets-path "/droid-icon-" r ".png")
		   (concat "droid_launcher.png"))
	(copy-file (concat droid-assets-path "/droid-icon-round-" r ".png")
		   (concat "droid_launcher_round.png"))))
    (with-droid-default-directory (concat app-path
					  "/app/src/main/res/mipmap-anydpi-v26")
      (droid-files-create-launcher-xml))
    (with-droid-default-directory (concat app-path
					  "/app/src/main/res/drawable")
      (copy-file (concat droid-assets-path "/droid-background.xml")
		 "droid_launcher_background.xml"))
        (with-droid-default-directory (concat app-path
					  "/app/src/main/res/drawable-v24")
      (copy-file (concat droid-assets-path "/droid-foreground.xml")
		 "droid_launcher_foreground.xml"))
    (with-droid-default-directory app-path
      (let ((report (make-progress-reporter "Initializing project..." nil nil)))
	(make-process :name "Droid Initialize"
		      :buffer nil
		      ;; The "tasks" task does not produce side effects
		      ;; but should still prepare the environment
		      :command (list (expand-file-name "gradlew") "tasks")
		      :filter (lambda (_a _b)
				(progress-reporter-update report))
		      :sentinel (lambda (_a string)
				  (progress-reporter-done report)
				  (unless (string= string "finished\n")
				    (error "Can't initialize Droid project")))))))
  nil)

;;;###autoload
(defun droid-import-project (path)
  "Place the project located at PATH under Droid."
  ;; Essentially it means adding a `droid--project-file-name' file on
  ;; the project's root directory.  Wether or not PATH is actually a
  ;; valid project is the user's business.
  (interactive "DProject: ")
  (unless (and (file-exists-p path) (directory-name-p path))
    (error "%s is not a valid project path" path))
  (with-droid-default-directory path
    (with-temp-file droid--project-file-name
      (insert "Made with Droid") (newline)
      (insert "Don't place this file under version control")
      (newline)))
  nil)

(provide 'droid)
;;; droid.el ends here
