;;; droid-files.el --- Create Droid project files    -*- lexical-binding: t; -*-

;; Copyright (C) 2018  Alessio Vanni

;; Author: Alessio Vanni <vannilla@firemail.cc>

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Create Droid project files.

;;; Code:

;; For some reasons, skeletons don't work here.
;; Thus, we'll have to manually `insert' every line.

(defun droid-files-create-build-gradle ()
  "Create a build.gradle file for an Android application.
The file is created inside `default-directory'."
  (with-temp-file "build.gradle"
    (insert "// Created by Droid")
    (newline)
    (insert "// These configurations apply to all subprojects and modules.")
    (newline 2)
    (insert "buildscript {")
    (newline)
    (insert "     repositories {")
    (newline)
    (insert "          google()")
    (newline)
    (insert "          jcenter()")
    (newline)
    (insert "     }")
    (newline 2)
    (insert "     dependencies {")
    (newline)
    (insert "          classpath 'com.android.tools.build:gradle:3.4.1'")
    (newline 2)
    (insert (concat "          // Your application's dependencies belong"
		    " in the individual module build.gradle, not here."))
    (newline)
    (insert "     }")
    (newline)
    (insert "}")
    (newline 2)
    (insert "allprojects {")
    (newline)
    (insert "     repositories {")
    (newline)
    (insert "          google()")
    (newline)
    (insert "          jcenter()")
    (newline)
    (insert "     }")
    (newline)
    (insert "}")
    (newline 2)
    (insert "task clean(type: Delete) {")
    (newline)
    (insert "     delete rootProject.buildDir")
    (newline)
    (insert "}")
    (newline)))

(defun droid-files-create-gradle-properties ()
  "Create a gradle.properties file for an Android application.
The file is created inside `default-directory'."
  (with-temp-file "gradle.properties"
    (insert "# Created by Droid")
    (newline)
    (insert "# Project-wide Gradle settings")
    (newline)
    (insert "# For more detailson how to configure your build environment visit")
    (newline)
    (insert "# http://www.gradle.org/docs/current/userguide/build_environment.html")
    (newline 2)
    (insert "# Specifies the JVM arguments used for the daemon process.")
    (newline)
    (insert "# The setting is particularly useful for tweaking memory settings.")
    (newline)
    (insert "org.gradle.jvmargs=-Xmx1536m")
    (newline 2)
    (insert "# When configured, Gradle will run in incubating parallel mode.")
    (newline)
    (insert (concat "# This option should only be used with decoupled projects."
		    " More details, visit"))
    (newline)
    (insert (concat "# http://www.gradle.org/docs/current/"
		    "userguide/multi_project_builds.html#sec:"
		    "decoupled_projects"))
    (newline)
    (insert "# org.gradle.parallel=true")
    (newline)))

(defun droid-files-create-settings-gradle ()
  "Create a settings.gradle file for an Android application.
The file is created inside `default-directory'."
  (with-temp-file "settings.gradle"
    (insert "// Created by Droid")
    (newline)
    (insert "include ':app'")
    (newline)))

(defun droid-files-create-local-properties (path)
  "Create a local.properties file for an Android application.
PATH is the path to the Android SDK (normally the value of `droid-sdk-path').
The file is created inside `default-directory'."
  (with-temp-file "local.properties"
    (insert "# Created by Droid")
    (newline)
    (insert "# Don't place this file under version control, as")
    (newline)
    (insert "# it contains informations specific to your local configuration.")
    (newline 2)
    (insert "# Path to the SDK. Used only by Gradle.")
    (newline)
    (insert (format "sdk.dir=%s" (expand-file-name path)))
    (newline)))

(defun droid-files-create-app-build-gradle (target package)
  "Create an application-specific build.gradle file.
TARGET is the SDK version targeted by the application.
PACKAGE is the application package.
The file is created inside `default-directory'."
  (with-temp-file "build.gradle"
    (insert "// Created by Droid")
    (newline)
    (insert "apply plugin: 'com.android.application'")
    (newline 2)
    (insert "android {")
    (newline)
    (insert (format "     compileSdkVersion %s" target))
    (newline)
    (insert "     defaultConfig {")
    (newline)
    (insert (format "          applicationId \"%s\"" package))
    (newline)
    (insert (format "          minSdkVersion %s" target))
    (newline)
    (insert (format "          targetSdkVersion %s" target))
    (newline)
    (insert "          versionCode 1")
    (newline)
    (insert "          versionName \"1.0\"")
    (newline)
    (insert "     }")
    (newline)
    (insert "     buildTypes {")
    (newline)
    (insert "          release {")
    (newline)
    (insert "               minifyEnabled false")
    (newline)
    (insert (concat "               proguardFiles getDefaultProguardFile("
		    "'proguard-android.txt'),"
		    "'proguard-rules.pro'"))
    (newline)
    (insert "          }")
    (newline)
    (insert "     }")
    (newline)
    (insert "}")
    (newline 2)
    (insert "dependencies {")
    (newline)
    (insert "     implementation fileTree(dir: 'libs', include: ['*.jar'])")
    (newline)
    (insert "}")
    (newline)))

(defun droid-files-create-app-proguard-rules ()
  "Create an application-specific proguard-rules.pro file.
The file is created inside `default-directory'."
  (with-temp-file "proguard-rules.pro"
    (insert "# Created by Droid")
    (newline)
    (insert "# Add project specific ProGuard rules here.")
    (newline)
    (insert "# You can control the set of applied configuration files using the")
    (newline)
    (insert "# proguardFiles setting in build.gradle.")
    (newline 2)
    (insert "# For more details, see")
    (newline)
    (insert "# http://developer.android.com/guide/developing/tools/proguard.html")
    (newline)))

(defun droid-files-create-manifest (package)
  "Create an AndroidManifest.xml file.
PACKAGE is the application's package.
The file is created inside `default-directory'."
  (with-temp-file "AndroidManifest.xml"
    (nxml-mode)
    (insert "<?xml version=\"1.0\" encoding=\"utf-8\"?>")
    (newline)
    (insert "<!-- Created by Droid -->")
    (newline)
    (insert (concat "<manifest xmlns:android=\"http://schemas.android.com"
		    "/apk/res/android\""))
    (newline)
    (insert (format "package=\"%s\">" package))
    (newline)
    (insert "<application")
    (newline)
    (insert "android:allowBackup=\"true\"")
    (newline)
    (insert "android:icon=\"@mipmap/droid_launcher\"")
    (newline)
    (insert "android:roundIcon=\"@mipmap/droid_launcher_round\"")
    (newline)
    (insert "android:label=\"@string/app_name\"")
    (newline)
    (insert "android:supportsRtl=\"true\"")
    (newline)
    (insert "android:theme=\"@style/AppTheme\">")
    (newline)
    (insert "<activity android:name=\".MainActivity\">")
    (newline)
    (insert "<intent-filter>")
    (newline)
    (insert "<action android:name=\"android.intent.action.MAIN\"/>")
    (newline)
    (insert "<category android:name=\"android.intent.category.LAUNCHER\"/>")
    (newline)
    (insert "</intent-filter>")
    (newline)
    (insert "</activity>")
    (newline)
    (insert "</application>")
    (newline)
    (insert "</manifest>")
    (newline)
    (indent-region (point-min) (point-max))))

(defun droid-files-create-activity (package)
  "Create a new Java class for an empty activity.
PACKAGE is the application's package.
The file is created inside `default-directory'."
  (with-temp-file "MainActivity.java"
    (java-mode)
    (insert (format "package %s;" package))
    (newline 2)
    (insert "import android.app.Activity;")
    (newline)
    (insert "import android.os.Bundle;")
    (newline 2)
    (insert "public class MainActivity extends Activity {")
    (newline)
    (insert "@Override")
    (newline)
    (insert "protected void onCreate(Bundle savedInstance) {")
    (newline)
    (insert "super.onCreate(savedInstance);")
    (newline)
    (insert "setContentView(R.layout.activity_main);")
    (newline)
    (insert "}")
    (newline)
    (insert "}")
    (newline)
    (indent-region (point-min) (point-max))))

(defun droid-files-create-layout-xml ()
  "Create a new activity_main.xml file.
The file is created inside `default-directory'."
  (with-temp-file "activity_main.xml"
    (nxml-mode)
    (insert "<?xml version=\"1.0\" encoding=\"utf-8\"?>")
    (newline)
    (insert "<!-- Created by Droid -->")
    (newline)
    (insert "<RelativeLayout")
    (newline)
    (insert "xmlns:android=\"http://schemas.android.com/apk/res/android\"")
    (newline)
    (insert "android:layout_width=\"match_parent\"")
    (newline)
    (insert "android:layout_height=\"match_parent\">")
    (newline)
    (insert "<TextView")
    (newline)
    (insert "android:layout_width=\"wrap_content\"")
    (newline)
    (insert "android:layout_height=\"wrap_content\"")
    (newline)
    (insert "android:text=\"@string/text_label\"/>")
    (newline)
    (insert "</RelativeLayout>")
    (newline)
    (indent-region (point-min) (point-max))))

(defun droid-files-create-string-xml (name)
  "Create a new strings.xml file.
NAME is the application's name.
The file is created inside `default-directory'."
  (with-temp-file "strings.xml"
    (nxml-mode)
    (insert "<!-- Created by Droid -->")
    (newline)
    (insert "<resources>")
    (newline)
    (insert (format "<string name=\"app_name\">%s</string>" name))
    (newline)
    (insert "<string name=\"text_label\">Hello, GNU Emacs!</string>")
    (newline)
    (insert "</resources>")
    (newline)
    (indent-region (point-min) (point-max))))

(defun droid-files-create-style-xml ()
  "Create a new styles.xml file.
The file is created inside `default-directory'."
  (with-temp-file "styles.xml"
    (nxml-mode)
    (insert "<!-- Created by Droid -->")
    (newline)
    (insert "<resources>")
    (newline)
    (insert (concat "<style name=\"AppTheme\" "
		    "parent=\"android:Theme.Material.Light\">"))
    (newline)
    (insert "<!-- Customize theme here -->")
    (newline)
    (insert "</style>")
    (newline)
    (insert "</resources>")
    (newline)
    (indent-region (point-min) (point-max))))

(defun droid-files-create-launcher-xml ()
    "Create new droid_launcher.xml and droid_launcher_round.xml files.
The files are created inside `default-directory'."
  (with-temp-file "droid_launcher.xml"
    (nxml-mode)
    (insert "<?xml version=\"1.0\" encoding=\"utf-8\"?>")
    (newline)
    (insert (concat "<adaptive-icon xmlns:android=\""
		    "http://schemas.android.com/apk/res/android\">"))
    (newline)
    (insert (concat "<background android:drawable=\""
		    "@drawable/droid_launcher_background\"/>"))
    (newline)
    (insert (concat "<foreground android:drawable=\""
		     "@drawable/droid_launcher_foreground\"/>"))
    (newline)
    (insert "</adaptive-icon>")
    (indent-region (point-min) (point-max)))
  (with-temp-file "droid_launcher_round.xml"
    (nxml-mode)
    (insert "<?xml version=\"1.0\" encoding=\"utf-8\"?>")
    (newline)
    (insert (concat "<adaptive-icon xmlns:android=\""
		    "http://schemas.android.com/apk/res/android\">"))
    (newline)
    (insert (concat "<background android:drawable=\""
		    "@drawable/droid_launcher_background\"/>"))
    (newline)
    (insert (concat "<foreground android:drawable=\""
		     "@drawable/droid_launcher_foreground\"/>"))
    (newline)
    (insert "</adaptive-icon>")
    (indent-region (point-min) (point-max))))

(provide 'droid-files)
;;; droid-files.el ends here
